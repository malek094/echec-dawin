#include <iostream>
#include "Joueur.h"
#include "Piece.h"

using namespace std;

Joueur::~Joueur()
{
  cout << (m_pieces[0].isWhite() ? "Destruction Joueur Blanc" : "Destruction Joueur Noir") << endl;
}

Joueur::Joueur(bool white)
{
  int n = 0;
  int y = white ? 1 : 8;
  for ( int x = 1; x <= 8; ++x )
    {
      m_pieces[ n ].init( x, y, white );
      n=n+1;
    }
  y = white ? 2 : 7;
  for ( int x = 1; x <= 8; ++x )
    m_pieces[ n++ ].init( x, y, white );
}

// n++ : retourner n puis n=n+1
// ++n : n=n+1 puis retourner n

void
Joueur::affiche()
{
  for (int i=0; i<16; i++)
    m_pieces[i].affiche();
}
